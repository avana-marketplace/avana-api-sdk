<?php

namespace Avana\SDK\Tests;

use Avana\SDK\Avana\V3\Client\AvanaClient;
use PHPUnit\Framework\TestCase;

class BaseTestCase extends TestCase
{
    protected $avanaShopId = '77377';
    protected $accessToken = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6Ijc2YWFkYWI5ODY4NWU2NTAxMDNkMTljZjYyODA1OTc4MjYwNjlmZDM0ODg5NDFmNGY3ZjUwMzQ1M2ZjMDMzZjJjNGFmMWY3MWE1YTRiMWI2In0.eyJhdWQiOiIxIiwianRpIjoiNzZhYWRhYjk4Njg1ZTY1MDEwM2QxOWNmNjI4MDU5NzgyNjA2OWZkMzQ4ODk0MWY0ZjdmNTAzNDUzZmMwMzNmMmM0YWYxZjcxYTVhNGIxYjYiLCJpYXQiOjE2MTU2MTY3OTEsIm5iZiI6MTYxNTYxNjc5MSwiZXhwIjoxNjQ3MTUyNzkxLCJzdWIiOiIxMDc1MjUiLCJzY29wZXMiOlsiYWxsIl19.jkERYSWP8orCAOst4QF0ETT_YTEgdr3fNA0NfxFMy_XvrkFsgrNdVa_cgblq5LbPArj3_8eFRCtjKcx2l-1W7rKJvxAxhgz1ZoBwyU0dW1lRLQVhziRKEvaMnmZsVSxhEdkl_omoqbXWTo84_GqB-N7LeZ-L5iv1ZHTXq5mBj5qKHs515n56ho3tRNObroAVaHC5_XFnN2OloRceh01paiFG4Nnz6djDD3jLPFNot_5UmOTi3ESL4A5_KiuPNmZx4psrzbXXahiQu-TiFbbC5cMIcv9mklHK1G-k_H5AunY28-yYyXmcGtxuq-1WrlXw-lls80hpMhA0FPr5OBFkZ2oU0ehXHto1HvIcbSfP40VmPvDoU6LyGmIbraSXF6vse4RTXyVDBctG_mbi831K-YPCtRRCG-tbwcaXDF6ae10kWxTEIdASC4Heg9H4VlC0nL-R4ppkiZKHNy47pOEGyrhafBYat2qwo-FJYMQmvpo0rChpOwsAPDYEXn-q2sslzwt7RFFjdMI_iEAWLEkPB8HsYNRngArQvwx0EEwSv69lXE8nhrZsoAUfQXg-NFBTxGnF0mxQxxaKunrzLfUGjJ2E7y2ogntCBiQ3MId2NotTWV22hSwGw8vd584agCtGoBdv8EKedxt-6VCQOs4h6AHSo3DTasz10B4bCFOogHk';
    protected $client;

    public function __construct(string $name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

        $this->client = new AvanaClient(
            [
            'base_uri'     => 'https://alpha-api.avana.link',
            'access_token' => $this->accessToken,
            ]
        );
    }


}
