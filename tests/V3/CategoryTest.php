<?php


namespace Avana\SDK\Tests\V3;

use Avana\SDK\Tests\BaseTestCase;

class CategoryTest extends BaseTestCase
{
    public function testGetCategoriesList()
    {
        $categoriesResponse = $this->client->category->getCategoriesList($this->avanaShopId);
        $this->assertNotNull($categoriesResponse);
        $this->assertArrayHasKey('flat', $categoriesResponse);
        $this->assertArrayHasKey('tree', $categoriesResponse);

        $this->assertTrue(count($categoriesResponse['flat']) > 0);
        $this->assertTrue(count($categoriesResponse['tree']) > 0);

        foreach ($categoriesResponse['flat'] as $category) {
            $this->assertArrayHasKey('id', $category);
            $this->assertArrayHasKey('name', $category);
            $this->assertArrayHasKey('sort_index', $category);
            $this->assertArrayHasKey('category_to_shop_id', $category);
            $this->assertArrayHasKey('parent_id', $category);
        }
    }
}
