<?php


namespace Avana\SDK\Tests\V3;

use Avana\SDK\Tests\BaseTestCase;

class ProductTest extends BaseTestCase
{
    public function testGetProductsList()
    {
        $productsListResponse = $this->client->product->getProductsList($this->avanaShopId);
        $this->assertNotNull($productsListResponse);
        $this->assertArrayHasKey('current_page', $productsListResponse);
        $this->assertArrayHasKey('data', $productsListResponse);
        foreach ($productsListResponse['data'] as $product) {
            $this->assertArrayHasKey('id', $product);
            $this->assertArrayHasKey('name', $product);
            $this->assertArrayHasKey('price', $product);
        }
    }

    public function testGetProductDetails()
    {
        $productsListResponse = $this->client->product->getProductsList($this->avanaShopId);
        $this->assertNotNull($productsListResponse);
        $this->assertArrayHasKey('data', $productsListResponse);
        if (count($productsListResponse['data']) > 0) {
            $productId              = $productsListResponse['data'][0]['id'];
            $productDetailsResponse = $this->client->product->getProductDetails($this->avanaShopId, $productId);
            $this->assertNotNull($productDetailsResponse);
            $this->assertArrayHasKey('id', $productDetailsResponse);
            $this->assertArrayHasKey('name', $productDetailsResponse);
            $this->assertArrayHasKey('price', $productDetailsResponse);
        }
    }

    public function testupdateShopeeQuantity()
    {
        $productsupdateShopeeQuantityResponse = $this->client->product->updateShopeeQuantity(
            $this->avanaShopId, [
            [
                'product_id' => '232498',
                'quantity' => 500
            ]
            ]
        );

        $this->assertNotNull($productsupdateShopeeQuantityResponse);
    }
}
