<?php


namespace Avana\SDK\Tests\V3;

use Avana\SDK\Tests\BaseTestCase;

class ProductVariationTest extends BaseTestCase
{
    public function testGetProductVariationsList()
    {
        $variationsList = $this->client->productVariation->getProductVariationsList($this->avanaShopId);
        $this->assertNotNull($variationsList);
        $this->assertArrayHasKey('data', $variationsList);

        foreach ($variationsList['data'] as $variation) {
            $this->assertArrayHasKey('id', $variation);
            $this->assertArrayHasKey('name', $variation);
            $this->assertArrayHasKey('options', $variation);
            $this->assertTrue(count($variation['options']) > 0);
            foreach ($variation['options'] as $option) {
                $this->assertArrayHasKey('id', $option);
                $this->assertArrayHasKey('name', $option);
            }
        }
    }
}
