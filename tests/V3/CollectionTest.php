<?php


namespace Avana\SDK\Tests\V3;

use Avana\SDK\Tests\BaseTestCase;

class CollectionTest extends BaseTestCase
{
    public function testGetCollectionsList()
    {
        $collectionResponse = $this->client->collection->getCollectionsList($this->avanaShopId);
        $this->assertNotNull($collectionResponse);
        foreach ($collectionResponse as $collection) {
            $this->assertArrayHasKey('label_id', $collection);
            $this->assertArrayHasKey('name', $collection);
            $this->assertArrayHasKey('slug', $collection);
            $this->assertArrayHasKey('description', $collection);
        }
    }
}
