<?php


namespace Avana\SDK\Tests\V3;

use Avana\SDK\Tests\BaseTestCase;

class ShopTest extends BaseTestCase
{
    public function testGetShopDetails()
    {
        $shopResponse = $this->client->shop->getShopDetails($this->avanaShopId);
        $this->assertNotNull($shopResponse);
        $this->assertArrayHasKey('shop_info', $shopResponse);
        $this->assertArrayHasKey('shop_name', $shopResponse['shop_info']);
    }
}
