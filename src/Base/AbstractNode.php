<?php

namespace Avana\SDK\Base;

use Avana\SDK\Avana\Exception\AvanaSDKException;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;

abstract class AbstractNode
{
    protected $httpClient  = null;
    protected $accessToken = null;

    public function __construct(array $config)
    {
        $this->httpClient = new Client(
            [
            'base_uri' => $config['base_uri']
            ]
        );

        $this->accessToken = $config['access_token'];
    }


    protected function postRequest($endPoint, $options = null)
    {
    }

    protected function getRequest($endPoint, $options = null)
    {
        try {
            $req = $this->httpClient->get(
                $endPoint, [
                'headers' => $this->getCommonHeaders(),
                ]
            );

            return $this->getDecodedContent($req->getBody());
        } catch (ClientException $e) {
            $exceptionContent = json_decode($e->getResponse()->getBody()->getContents());
            switch ($e->getCode()) {
            case 401:
                throw new AvanaSDKException($exceptionContent->message, 401);

            default:
                throw new \Exception($exceptionContent->message, $e->getCode());
            };
        }
    }


    protected function putRequest($endPoint, $parameters = [], $avanaToken = null)
    {
        try {
            $req = $this->httpClient->put(
                $endPoint, [
                'headers'         => $this->getCommonHeaders($avanaToken),
                'query'            => $parameters
                ]
            );

            return $this->getDecodedContent($req->getBody());
        } catch (ClientException $e) {
            $exceptionContent = json_decode($e->getResponse()->getBody()->getContents());
            switch ($e->getCode()) {
            case 401:
                throw new AvanaSDKException($exceptionContent->message, 401);

            default:
                throw new \Exception($exceptionContent->message, $e->getCode());
            };
        }
    }

    protected function getDecodedContent($response)
    {
        return json_decode($response, true);
    }

    protected function getCommonHeaders($avanaToken = null): array
    {
        if(is_null($avanaToken)) {
            $avanaToken = $this->accessToken;
        }else{
            $avanaToken = $avanaToken;
        }
        return [
            'Content-Type'  => 'application/son',
            'Authorization' => 'Bearer '.$avanaToken,
        ];
    }
}
