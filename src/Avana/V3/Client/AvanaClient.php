<?php


namespace Avana\SDK\Avana\V3\Client;

use Avana\SDK\Avana\Base\IAvanaClient;
use Avana\SDK\Avana\V3\Services\CategoryService;
use Avana\SDK\Avana\V3\Services\CollectionService;
use Avana\SDK\Avana\V3\Services\ProductService;
use Avana\SDK\Avana\V3\Services\ProductVariationService;
use Avana\SDK\Avana\V3\Services\ShopService;

class AvanaClient implements IAvanaClient
{
    public $shop             = null;
    public $product          = null;
    public $category         = null;
    public $collection       = null;
    public $productVariation = null;

    public function __construct(array $config)
    {
        $this->shop             = new ShopService($config);
        $this->product          = new ProductService($config);
        $this->category         = new CategoryService($config);
        $this->collection       = new CollectionService($config);
        $this->productVariation = new ProductVariationService($config);
    }
}
