<?php


namespace Avana\SDK\Avana\V3\Services;

use Avana\SDK\Base\AbstractNode;

class ProductService extends AbstractNode
{
    const SORT_DIRECTION_ASC  = 'asc';
    const SORT_DIRECTION_DESC = 'desc';

    const AVAILABILITY_ALL          = 'all';
    const AVAILABILITY_AVAILABLE    = 'available';
    const AVAILABILITY_OUT_OF_STOCK = 'out-of-stock';

    const SORT_KEY_NAME       = 'name';
    const SORT_KEY_PRICE      = 'price';
    const SORT_KEY_SALE_PRICE = 'sale_price';

    public function getProductDetails($shopId, $productId)
    {
        return $this->getRequest(sprintf('/api/v3/shops/%s/products/%s', $shopId, $productId));
    }

    public function getProductsList(
        $shopId,
        $page = 1,
        $perPage = 10,
        $excludeIds = [],
        $filterByName = '',
        $sortKey = '',
        $sortDirection = self::SORT_DIRECTION_ASC,
        $availability = self::AVAILABILITY_ALL,
        $filterByCategories = []
    ) {
    
        $categoryQueryParam = '';
        foreach ($filterByCategories as $categoryId) {
            $categoryQueryParam .= sprintf('&categories[]=%s', $categoryId);
        }
        $excludeIdsParam = '';
        foreach ($excludeIds as $excludeId) {
            $excludeIdsParam .= sprintf('&exclude_ids[]=%s', $excludeId);
        }
        return $this->getRequest(
            sprintf(
                '/api/v3/shops/%s/products?page=%s&limit=%s&name=%s&sortKey=%s&sortValue=%s&availability=%s%s%s',
                $shopId,
                $page,
                $perPage,
                $filterByName,
                $sortKey,
                $sortDirection,
                $availability,
                $categoryQueryParam,
                $excludeIdsParam
            )
        );
    }
    
    public function updateShopeeQuantity($shopId, $parameters, $avanaToken = null)
    {
        return $this->putRequest(
            sprintf(
                '/api/v3/shops/%s/marketplaces/%s/products/deduct-origin',
                $shopId,
                'shopee',
            ), $parameters, $avanaToken
        );
    }
}
