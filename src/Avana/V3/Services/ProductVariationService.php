<?php


namespace Avana\SDK\Avana\V3\Services;

use Avana\SDK\Base\AbstractNode;

class ProductVariationService extends AbstractNode
{
    public function getProductVariationsList($shopId)
    {
        return $this->getRequest(sprintf('/api/v3/shops/%s/product-variations', $shopId));
    }
}
