<?php


namespace Avana\SDK\Avana\V3\Services;

use Avana\SDK\Base\AbstractNode;

class CategoryService extends AbstractNode
{
    public function getCategoriesList($shopId)
    {
        return $this->getRequest(sprintf('/api/v3/shops/%s/product-categories', $shopId));
    }
}
