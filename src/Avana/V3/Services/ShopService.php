<?php


namespace Avana\SDK\Avana\V3\Services;

use Avana\SDK\Base\AbstractNode;

class ShopService extends AbstractNode
{
    public function getShopDetails($shopID)
    {
        return $this->getRequest(sprintf('/api/v3/shops/%s', $shopID));
    }
}
