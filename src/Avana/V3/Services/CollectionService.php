<?php


namespace Avana\SDK\Avana\V3\Services;

use Avana\SDK\Base\AbstractNode;

class CollectionService extends AbstractNode
{
    public function getCollectionsList($shopId)
    {
        return $this->getRequest(sprintf('/api/v3/shops/%s/product-collections', $shopId));
    }
}
